﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShipAreaManager : MonoBehaviour
{
    public int totalCompnents = 5;
    private int collectedComponents = 0;
    // Start is called before the first frame update
    public string collectedPartString;
    public Text collectedPartText;
    void Start()
    {
        collectedPartString = totalCompnents + "/" + collectedComponents;
        collectedPartText.text = "Collected Parts:" + collectedPartString;
    }


    public void placePart(GameObject part)
    {
        //part.tag = "PlacedPart";
        collectedComponents += 1;
        collectedPartString = totalCompnents + "/" + collectedComponents;
        collectedPartText.text = "Collected Parts:" + collectedPartString;
        Destroy(part);
        if(collectedComponents == totalCompnents)
        {
            SceneManager.LoadScene("EndCinematic");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //if(other.CompareTag("Part"))
        //{
        //    print(other.GetComponent<Part>().isCarried);
        //    if (other.GetComponent<Part>().isCarried == false)
        //    {
        //        collectedComponents += 1;
        //        other.GetComponent<Collider>().enabled = false;
        //    }
        //}
    }
}
