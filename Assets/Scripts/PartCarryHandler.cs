﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartCarryHandler : MonoBehaviour
{
    private bool amITouchingAPart = false;

    private GameObject touchedObject = null;
    public bool carrying = false;
    // Update is called once per frame
    private bool isInShipArea = false;
    private ShipAreaManager shipAreaManager;

    private void Start()
    {
        shipAreaManager = GameObject.FindGameObjectWithTag("ShipArea").GetComponent<ShipAreaManager>();
    }
    void Update()
    {
        //print(amITouchingAPart);
        if(amITouchingAPart && Input.GetKey("e") && touchedObject != null && !carrying)
        {
            print("carrryyy");
            touchedObject.transform.parent = transform;
            carrying = true;
            touchedObject.GetComponent<Part>().isCarried = true;
        }

        if (carrying && Input.GetKey("r") && touchedObject != null)
        {
            if (isInShipArea)
            {
                shipAreaManager.placePart(touchedObject);
            }
            touchedObject.GetComponent<Part>().isCarried = false;
            print("throww" + "touchedObject.GetComponent<Part>().isCarried" + touchedObject.GetComponent<Part>().isCarried);
            touchedObject.transform.parent = null;
            
            carrying = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ShipArea"))
        {
            isInShipArea = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("ShipArea"))
        {
            isInShipArea = false;
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Part") && !carrying)
        {
            amITouchingAPart = true;
            touchedObject = collision.gameObject;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.CompareTag("Part") && !carrying)
        {
            touchedObject = null;
            amITouchingAPart = false;
        }
    }
}
