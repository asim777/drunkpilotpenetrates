﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject settings;
    public GameObject mainmusic;

    AudioSource m_MyAudioSource;
    //Play the music
    bool m_Play;

    void Start()
    {
        //Fetch the AudioSource from the GameObject
        m_MyAudioSource = GetComponent<AudioSource>();
        //Ensure the toggle is set to true for the music to play at start-up
        m_Play = true;
    }

    // Starting new game
    public void StartGame()
    {
        SceneManager.LoadScene(2);
        GameObject.DontDestroyOnLoad(mainmusic);
    }

    // Music on/off and other settings
    public void Settings()
    {
        settings.SetActive(!settings.activeSelf);
    }

    // Exit from our game
    public void ExitGame()
    {
        Application.Quit();
    }

    // Set for Sound and Music
    public void setMusic(float musicvalue)
    {
        Global.music = musicvalue;
        if(musicvalue == 0)
        {
            //Stop the audio
            m_MyAudioSource.Stop();
        }
    }

    public void setSound(float soundvalue)
    {
        Global.sound = soundvalue;
    }
}
