﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OxygenBar : MonoBehaviour
{
    public GameObject SphereEnter;
    public float percentageOfOxygenConsumption = 5;

    public Image oxygenBar;
    public float maxOxygen = 100f;
    public static float oxygen;
    public bool isInsideDome;

    // Start is called before the first frame update
    public void Start()
    {
        oxygenBar = GetComponent<Image>();
        oxygen = maxOxygen;
    }

    // Update is called once per frame
    public void Update()
    {
        oxygenBar.fillAmount = oxygen / maxOxygen;

        if (isInsideDome == false) {
            oxygen -= percentageOfOxygenConsumption / 100f;
        } else {
            oxygen += percentageOfOxygenConsumption*2 / 100f;
        }

        if (oxygenBar.fillAmount == 0) {
            Application.LoadLevel(4);
        }
    }

    public void OnTriggerEnter(Collider SphereEnter)
    {
        oxygen = maxOxygen;
    }
}

