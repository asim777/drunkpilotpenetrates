﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OxygenZoneEnter : MonoBehaviour
{
    public GameObject oxygenRegeneration;
    OxygenBar oxx = new OxygenBar();

    public void OnTriggerEnter(Collider other)
    {
        //Debug.Log("WORK");
        oxx.percentageOfOxygenConsumption = 0;
        oxx.oxygenBar.fillAmount += 1;
    }
}
