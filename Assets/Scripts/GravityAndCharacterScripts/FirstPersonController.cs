﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (GravityBody))]
public class FirstPersonController : MonoBehaviour {
	
	// public vars
	public float mouseSensitivityX = 1;
	public float mouseSensitivityY = 1;
	public float walkSpeed = 6;
	public float jumpForce = 220;
	public LayerMask groundedMask;
	
	// System vars
	bool grounded;
	Vector3 moveAmount;
	Vector3 smoothMoveVelocity;
	float verticalLookRotation;
	Transform cameraTransform;
	Rigidbody rigidbody;
	public Animator animator;
	private PartCarryHandler partCarryHandler;
	private OxygenBar oxygenBar;
	
	void Awake() {
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		partCarryHandler = gameObject.GetComponent<PartCarryHandler>();
		cameraTransform = Camera.main.transform;
		rigidbody = GetComponent<Rigidbody> ();
        oxygenBar = GameObject.Find("OxygenBar").GetComponent<OxygenBar>();
	}
	
	void Update() {
		transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * mouseSensitivityX);
		//verticalLookRotation += Input.GetAxis("Mouse Y") * mouseSensitivityY;
		//verticalLookRotation = Mathf.Clamp(verticalLookRotation,-60,60);
		//cameraTransform.localEulerAngles = Vector3.left * verticalLookRotation;
		//print(amITouchingAPart);
		// Calculate movement:
		float inputX = Input.GetAxisRaw("Horizontal");
		float inputY = Input.GetAxisRaw("Vertical");
		
		Vector3 moveDir = new Vector3(inputX,0, inputY).normalized;
		Vector3 targetMoveAmount = moveDir * walkSpeed;
		moveAmount = Vector3.SmoothDamp(moveAmount,targetMoveAmount,ref smoothMoveVelocity,.15f);
		
		// Jump
		if (Input.GetButtonDown("Jump") && partCarryHandler.carrying == false) {
			animator.Play("Armature|Jump_Start", 0, 4f);
			if (grounded) {
				rigidbody.AddForce(transform.up * jumpForce);
			}
		}
		
		// Grounded check
		Ray ray = new Ray(transform.position, -transform.up);
		RaycastHit hit;
		
		if (Physics.Raycast(ray, out hit, 1 + .1f, groundedMask)) {
			if (grounded == false)
			{
				animator.SetBool("isGrounded", true);
			}
			grounded = true;
		}
		else {
			grounded = false;
		}

		if (Input.GetKey("w") || Input.GetKey("a") || Input.GetKey("s") || Input.GetKey("d")
            || Input.GetKey("left") || Input.GetKey("down") || Input.GetKey("up") || Input.GetKey("right"))
		{
            if(partCarryHandler.carrying)
            {
				animator.SetBool("canCarry", true);
			}
            else
            {
				animator.SetBool("canRun", true);
			}
			
		}
		else
		{
			animator.SetBool("canCarry", false);
			animator.SetBool("canRun", false);
		}
	}
	
	void FixedUpdate() {
		// Apply movement to rigidbody
		Vector3 localMove = transform.TransformDirection(moveAmount) * Time.fixedDeltaTime;
		rigidbody.MovePosition(rigidbody.position + localMove);
	}

    private void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.tag == "OxygenSphere")
        {
			oxygenBar.isInsideDome = true;
			print("inside dome");
        }

	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "OxygenSphere")
		{
			oxygenBar.isInsideDome = false;
			print("outside dome");
		}

	}


	//   private void OnTriggerEnter(Collider other)
	//   {
	//	print("exit" + other.transform.tag);
	//	if (other.transform.CompareTag("Part"))
	//	{
	//		amITouchingAPart = true;
	//	}
	//}

	//private void OnTriggerExit(Collider other)
	//{
	//	print(other.transform.tag);
	//	if (other.transform.CompareTag("Part"))
	//	{
	//		amITouchingAPart = false;
	//	}
	//}
}
